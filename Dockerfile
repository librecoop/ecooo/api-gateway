FROM python:3.10-slim-bullseye

WORKDIR /code
COPY requirements.txt /code/requirements.txt
RUN pip install --no-cache-dir --upgrade -r requirements.txt
COPY ./ /code
EXPOSE 8000
CMD ["uvicorn", "src.main:app", "--proxy-headers", "--host", "0.0.0.0", "--log-level", "info"]
