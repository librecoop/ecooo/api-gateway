import pytest
from fastapi.testclient import TestClient
from src.main import app

client = TestClient(app)


@pytest.mark.parametrize('amount,order', [(200, "000111333")])
def test_generate_signature(mocker, amount, order):
    response = client.get('/payments/signature', params={"amount": amount, "order": order}) 
    assert response.status_code == 200
    result = response.json()
    assert result["Ds_SignatureVersion"] == "HMAC_SHA256_V1"
    assert result["Ds_MerchantParameters"] == "eyJEc19NZXJjaGFudF9NZXJjaGFudENvZGUiOiAiOTk5MDA4ODgxIiwgIkRzX01lcmNoYW50X1Rlcm1pbmFsIjogIjEiLCAiRHNfTWVyY2hhbnRfVHJhbnNhY3Rpb25UeXBlIjogIjAiLCAiRHNfTWVyY2hhbnRfQ3VycmVuY3kiOiA5NzgsICJEc19NZXJjaGFudF9PcmRlciI6ICIwMDAxMTEzMzMiLCAiRHNfTWVyY2hhbnRfQW1vdW50IjogMjAwMDAsICJEc19NZXJjaGFudF9NZXJjaGFudERhdGEiOiAiSW52ZXN0bWVudCBQYXltZW50IiwgIkRzX01lcmNoYW50X01lcmNoYW50TmFtZSI6ICJFY29vbyIsICJEc19NZXJjaGFudF9UaXR1bGFyIjogIkVjb29vIEVuZXJnXHUwMGVkYSBDaXVkYWRhbmEgUy5DT09QLiIsICJEc19NZXJjaGFudF9Qcm9kdWN0RGVzY3JpcHRpb24iOiAiSW52ZXJzaVx1MDBmM24gZGUgMjAwXHUyMGFjIGVuIEVjb29vIiwgIkRzX01lcmNoYW50X01lcmNoYW50VVJMIjogImh0dHBzOi8vZWNvb28uZXMvIiwgIkRzX01lcmNoYW50X1VybE9LIjogImh0dHA6Ly9sb2NhbGhvc3Q6MzAwMC9hcmVhLXBlcnNvbmFsL2ludmVyc2lvbmVzP3BheW1lbnQ9c3VjY2Vzc2Z1bCJ9"
    assert result["Ds_Signature"] == "htgg+2x2TG6t/iFU93YA9tTnwIbF7zf3W/Uj+LHOoWE="  
