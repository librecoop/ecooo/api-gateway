from fastapi.testclient import TestClient
import pytest
from src.main import app
import json
from requests import Response


client = TestClient(app)


@pytest.mark.parametrize('autoconsumption_estimate_path', [('./tests/fixtures/autoconsumption/autoconsumption_estimate.json')])
def test_create_contact(mocker, autoconsumption_estimate_path):
    mocker.patch(
        'src.utils.odoo_rest.send_post',
        return_value={"result": {"code": '200', "partner_id": 1, "partner_email": "test@mail.com"}}
    )

    mocker.patch(
        'mailchimp_marketing.api.lists_api.ListsApi.batch_list_members',
        return_value=True
    )

    with open(autoconsumption_estimate_path) as json_file:
        json_data = json.load(json_file)
    response = client.post('/autoconsumption/estimate', json=json_data)
    assert response.status_code == 200, response.text
    result = response.json()
    assert 'partner_id' in result and 'partner_email' in result


def test_create_contact_error(mocker):
    mocker.patch(
        'src.utils.odoo_rest.send_post',
        return_value={"result": {"code": "400", "message": "Partner data error", "error": "There was an error setting values"}}
    )

    with open('./tests/fixtures/autoconsumption/autoconsumption_estimate.json') as json_file:
        json_data = json.load(json_file)
    response = client.post('/autoconsumption/estimate', json=json_data)
    assert response.status_code == 400, str(response.json())

@pytest.mark.parametrize('fixture_path,endpoint_path,field,expected_code', [
    # Aerothermal info tests
    ('./tests/fixtures/autoconsumption/aerothermal_info.json','/autoconsumption/aerothermal/info',None, 200),
    ('./tests/fixtures/autoconsumption/aerothermal_info.json','/autoconsumption/aerothermal/info','firstname', 422),
    ('./tests/fixtures/autoconsumption/aerothermal_info.json','/autoconsumption/aerothermal/info','lastname', 422),
    ('./tests/fixtures/autoconsumption/aerothermal_info.json','/autoconsumption/aerothermal/info','phone', 422),
    ('./tests/fixtures/autoconsumption/aerothermal_info.json','/autoconsumption/aerothermal/info','email', 422),
    ('./tests/fixtures/autoconsumption/aerothermal_info.json','/autoconsumption/aerothermal/info','street', 422),
    ('./tests/fixtures/autoconsumption/aerothermal_info.json','/autoconsumption/aerothermal/info','state', 422),
    ('./tests/fixtures/autoconsumption/aerothermal_info.json','/autoconsumption/aerothermal/info','zip', 422),
    ('./tests/fixtures/autoconsumption/aerothermal_info.json','/autoconsumption/aerothermal/info','house_type', 422),
    ('./tests/fixtures/autoconsumption/aerothermal_info.json','/autoconsumption/aerothermal/info','climate_control_system', 422),
    ('./tests/fixtures/autoconsumption/aerothermal_info.json','/autoconsumption/aerothermal/info','tags', 200),
    ('./tests/fixtures/autoconsumption/aerothermal_info.json','/autoconsumption/aerothermal/info','participation_reason', 200),
    ('./tests/fixtures/autoconsumption/aerothermal_info.json','/autoconsumption/aerothermal/info','about_us', 200),
    ('./tests/fixtures/autoconsumption/aerothermal_info.json','/autoconsumption/aerothermal/info','personal_data_policy', 422),
    ('./tests/fixtures/autoconsumption/aerothermal_info.json','/autoconsumption/aerothermal/info','promotions', 200),
    # OTC assitance tests
    ('./tests/fixtures/autoconsumption/otc_assistance.json','/autoconsumption/otc/assistance',None,200),
    ('./tests/fixtures/autoconsumption/otc_assistance.json','/autoconsumption/otc/assistance','firstname',422),
    ('./tests/fixtures/autoconsumption/otc_assistance.json','/autoconsumption/otc/assistance','street',200),
    ('./tests/fixtures/autoconsumption/otc_assistance.json','/autoconsumption/otc/assistance','zip',200),
    ('./tests/fixtures/autoconsumption/otc_assistance.json','/autoconsumption/otc/assistance','city',422),
    ('./tests/fixtures/autoconsumption/otc_assistance.json','/autoconsumption/otc/assistance','email',422),
    ('./tests/fixtures/autoconsumption/otc_assistance.json','/autoconsumption/otc/assistance','website',200),
    ('./tests/fixtures/autoconsumption/otc_assistance.json','/autoconsumption/otc/assistance','social_media',200),
    ('./tests/fixtures/autoconsumption/otc_assistance.json','/autoconsumption/otc/assistance','status',200),
    ('./tests/fixtures/autoconsumption/otc_assistance.json','/autoconsumption/otc/assistance','legal_form',200),
    ('./tests/fixtures/autoconsumption/otc_assistance.json','/autoconsumption/otc/assistance','size',200),
    ('./tests/fixtures/autoconsumption/otc_assistance.json','/autoconsumption/otc/assistance','sponsors',200),
    ('./tests/fixtures/autoconsumption/otc_assistance.json','/autoconsumption/otc/assistance','commite_size',200),
    ('./tests/fixtures/autoconsumption/otc_assistance.json','/autoconsumption/otc/assistance','representation',200),
    ('./tests/fixtures/autoconsumption/otc_assistance.json','/autoconsumption/otc/assistance','main_interests',200),
    ('./tests/fixtures/autoconsumption/otc_assistance.json','/autoconsumption/otc/assistance','secondary_interests',200),
    ('./tests/fixtures/autoconsumption/otc_assistance.json','/autoconsumption/otc/assistance','about_us',200),
    ('./tests/fixtures/autoconsumption/otc_assistance.json','/autoconsumption/otc/assistance','objectives',200),
    ('./tests/fixtures/autoconsumption/otc_assistance.json','/autoconsumption/otc/assistance','personal_data_policy',200),
    ('./tests/fixtures/autoconsumption/otc_assistance.json','/autoconsumption/otc/assistance','promotions',200),
    ('./tests/fixtures/autoconsumption/otc_assistance.json','/autoconsumption/otc/assistance','tags',200),
    ('./tests/fixtures/autoconsumption/otc_assistance.json','/autoconsumption/otc/assistance','promotions_str',200),
    ('./tests/fixtures/autoconsumption/otc_assistance.json','/autoconsumption/otc/assistance','personal_data_policy_str',200),
    ('./tests/fixtures/autoconsumption/otc_assistance.json','/autoconsumption/otc/assistance','tags_str',200),
    # Energy certificate tests
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', None, 200),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "firstname", 422),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "lastname", 422),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "vat", 422),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "street", 422),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "city", 422),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "state", 422),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "zip", 422),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "email", 422),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "phone", 422),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "personal_data_policy", 200),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "promotions", 200),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "home_ownership", 422),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "cadastral_reference", 200),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "number_of_residents", 422),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "n_small_window_north", 422),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "n_small_window_south", 422),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "n_small_window_east", 422),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "n_small_window_west", 422),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "n_medium_window_north", 422),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "n_medium_window_south", 422),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "n_medium_window_east", 422),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "n_medium_window_west", 422),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "n_big_window_north", 422),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "n_big_window_south", 422),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "n_big_window_east", 422),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "n_big_window_west", 422),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "n_glassed_doors_north", 422),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "n_glassed_doors_south", 422),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "n_glassed_doors_east", 422),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "n_glassed_doors_west", 422),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "window_types", 422),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "window_description", 200),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "glass_types", 422),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "glass_description", 200),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "frame_types", 422),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "frame_description", 200),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "window_extra_info", 200),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "n_balcony_north", 422),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "n_balcony_south", 422),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "n_balcony_east", 422),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "n_balcony_west", 422),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "heating_system", 422),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "heating_type", 422),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "heating_use", 422),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "heating_age", 422),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "heating_power", 422),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "water_tank", 200),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "n_heating_devices", 422),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "water_heating_type", 200),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "water_heating_age", 422),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "water_heating_power", 200),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "accumulation_tank", 200),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "refrigeration_type", 422),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "n_refrigeration_devices", 200),
    ('./tests/fixtures/autoconsumption/energy_certificate.json', '/autoconsumption/energy-certificate', "refrigeration_age", 200),
])
def test_autoconsumption_endpoints_required_fields(mocker, fixture_path, endpoint_path, field, expected_code):
    mocked_response = Response()
    mocked_response.status_code=200
    mocker.patch(
        'src.utils.odoo_rest.send_post_api',
        return_value=mocked_response
    )

    mocker.patch(
        'mailchimp_marketing.api.lists_api.ListsApi.batch_list_members',
        return_value=True
    )
    with open(fixture_path) as json_file:
        json_data = json.load(json_file)
    if field != None:
        del json_data[field]
    response = client.post(endpoint_path, json=json_data)
    print(response)
    assert response.status_code == expected_code, str(response.json())
