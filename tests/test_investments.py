from fastapi.testclient import TestClient
import pytest
from src.main import app
import json


client = TestClient(app)


@pytest.mark.parametrize(
    'contract_path',
    [
        './tests/fixtures/investment/individual.json',
        './tests/fixtures/investment/minor.json',
        './tests/fixtures/investment/marriage.json',
        './tests/fixtures/investment/partnership.json'
    ]
)
def test_create_contract(mocker, contract_path):
    mocker.patch(
        'src.utils.odoo_rest.send_post',
        return_value={"result": {"code": '200', "partner_id": 1, "contract_id": 123, "partner_vat": '34632524F'}}
    )

    mocker.patch(
        'mailchimp_marketing.api.lists_api.ListsApi.batch_list_members',
        return_value=True
    )

    with open(contract_path) as json_file:
        json_data = json.load(json_file)
    response = client.post(
        '/investments/',
        json=json_data
    )
    assert response.status_code == 200, str(response.json())
    result = response.json()
    assert 'contract_id' in result and 'partner_id' in result and 'partner_vat' in result


def test_create_contract_error(mocker):
    mocker.patch(
        'src.utils.odoo_rest.send_post',
        return_value={"result": {"code": "400", "message": "Partner data error", "error": "The state of the partner  differs from that in location None"}}
    )

    with open('./tests/fixtures/investment/individual.json') as json_file:
        json_data = json.load(json_file)
    response = client.post('/investments/', json=json_data)
    assert response.status_code == 400, str(response.json())
