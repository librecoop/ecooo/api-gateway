from fastapi.testclient import TestClient
import pytest
from src.main import app
import json


client = TestClient(app)


@pytest.mark.parametrize(
    'contact_path',
    [
        './tests/fixtures/contact/contact.json',
        './tests/fixtures/contact/colective_autoconsumption_info.json',
        './tests/fixtures/contact/colective_autoconsumption_estimate.json',
        './tests/fixtures/contact/autoconsumption_info.json'
    ]
)
def test_create_contact(mocker, contact_path):
    mocker.patch(
        'src.utils.odoo_rest.send_post',
        return_value={"result": {"code": '200', "partner_id": 1, "partner_email": "test@mail.com"}}
    )

    mocker.patch(
        'mailchimp_marketing.api.lists_api.ListsApi.batch_list_members',
        return_value=True
    )

    with open(contact_path) as json_file:
        json_data = json.load(json_file)
    response = client.post('/contacts/', json=json_data)
    assert response.status_code == 200, response.text
    result = response.json()
    assert 'partner_id' in result and 'partner_email' in result


def test_create_contact_error(mocker):
    mocker.patch(
        'src.utils.odoo_rest.send_post',
        return_value={"result": {"code": "400", "message": "Partner data error", "error": "There was an error setting values"}}
    )

    with open('./tests/fixtures/contact/contact.json') as json_file:
        json_data = json.load(json_file)
    response = client.post('/contacts/', json=json_data)
    assert response.status_code == 400, str(response.json())
