from fastapi.testclient import TestClient
import pytest
from src.main import app


client = TestClient(app)


@pytest.mark.parametrize('json_data', [
    {'type': 'payment-confirmation', 'recipient_name': 'Javier', 'recipient_mail': 'test@mail.com'},
    {'type': 'transfer-reminder', 'recipient_name': 'Javier', 'recipient_mail': 'test@mail.com'}
])
def test_send_message(mocker, json_data):
    mocker.patch('requests.post', return_value=True)
    mocker.patch('src.utils.odoo_rest.send_get', return_value={'response': [{'email': 'test@mail.com'}]})
    response = client.post('/messages/', json=json_data)
    assert response.status_code == 200, response.text
