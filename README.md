# API Gateway
_Unified API Gateway for all of Ecooo's infrastructure_

## Setup Instructions

Default setup and settings are prepared for local development - better in a Linux machine :)

0. (Optional) Set up a python3 virtual environment
```shell
virtualenv -p python3 .venv
```
Note: (Windows users might prefer to use **Conda** for this)

1. Activate python venv
```shell
source .venv/bin/activate
```

2. Gather python requirements
```shell
pip install -r requirements.txt
```
3. Start the server with
```shell
uvicorn main:app --reload
```

To view the api documentation go to http://127.0.0.1:8000/docs

## Running the unit tests

To run the unit test created with pytest the following command must be run (withthe virtual environment activated)
```shell
pytest
```
