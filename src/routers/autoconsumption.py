from fastapi import APIRouter, HTTPException
from mailchimp_marketing.api_client import ApiClientError

from src.models.otc_assistance import OTCAssistance
from src.models.aerothermal_info import AerothermalInfo
from src.models.autoconsumption_estimate import AutoconsumptionEstimate
from src.models.selfconsumption_aquisition import SelfconsumptionAquisition
from src.models.energy_certificate import EnergyCertificate
from src.utils import odoo_rest
from src.utils.logged_router import LoggedRoute
from src.utils import mailchimp
from src.utils.mappers import map_strigified_fields
from datetime import datetime

router = APIRouter(
    prefix="/autoconsumption",
    tags=["Autoconsumption", "Odoo"],
    route_class=LoggedRoute
)

SELFCONSUMPTION_AQUISITION_EXCLUDE_FIELDS = {
    'selconsumption_type',
    'selfconsumption_aquisition',
    'building_role',
    'building_approval'
}

OTC_ASSISTANCE_EXLCUDE_FIELDS = {
    "website",
    "social_media",
    "status",
    "legal_form",
    "size",
    "sponsors",
    "commite_size",
    "representation",
    "main_interests",
    "secondary_interests",
    "about_us", "objectives",
    "tags_str",
    "personal_data_policy_str",
    "promotions_str"
}

ENERGY_CERTIFICATE_EXCLUDE_FIELDS = {
    "home_ownership",
    "cadastral_reference",
    "number_of_residents",
    "n_small_window_north",
    "n_small_window_south",
    "n_small_window_east",
    "n_small_window_west",
    "n_medium_window_north",
    "n_medium_window_south",
    "n_medium_window_east",
    "n_medium_window_west",
    "n_big_window_north",
    "n_big_window_south",
    "n_big_window_east",
    "n_big_window_west",
    "window_types",
    "window_description",
    "glass_types",
    "glass_description",
    "frame_types",
    "frame_description",
    "window_extra_info",
    "n_balcony_north",
    "n_balcony_south",
    "n_balcony_east",
    "n_balcony_west",
    "heating_system",
    "heating_type",
    "heating_use",
    "heating_age",
    "heating_power",
    "water_tank",
    "n_heating_devices",
    "water_heating_type",
    "water_heating_age",
    "water_heating_power",
    "accumulation_tank",
    "refrigeration_type",
    "n_refrigeration_devices",
    "refrigeration_age"
}


@router.post("/estimate", operation_id='post_estimate')
def post_estimate(autoconsumption_estimate: AutoconsumptionEstimate):
    result = odoo_rest.send_post('partner/create', autoconsumption_estimate)['result']
    if (int(result.get('code')) == 200):
        if (autoconsumption_estimate.promotions):
            try:
                mailchimp.subscribe_to_mailchimp(autoconsumption_estimate.email)
            except ApiClientError as error:
                raise HTTPException(status_code=error.status_code, detail=error.text)
        return result
    raise HTTPException(status_code=int(result.get('code')), detail=result.get('error'))


@router.post("/aquisition", operation_id='post_aquisition')
def post_aquisition(selfconsumption_aquisition: SelfconsumptionAquisition):
    data = selfconsumption_aquisition.dict(exclude_none=True, exclude_unset=True, exclude=SELFCONSUMPTION_AQUISITION_EXCLUDE_FIELDS)
    data['comment'] = (
        f'¿Tienes algun rol en el inmueble?: {selfconsumption_aquisition.building_role}\n'
        f'¿Tienes la aprobación de la junta de vecinos para el uso de la cubierta?: {selfconsumption_aquisition.building_approval}\n'
        f'¿Qué tipo de autoconsumo tendría tu hogar?: {selfconsumption_aquisition.selconsumption_type}\n'
        f'¿A que compra colectiva te quieres unir?: {selfconsumption_aquisition.selfconsumption_aquisition}\n'
    )
    result = odoo_rest.send_post_api('contacts', data)
    if (result.ok):
        return 'Succesfull request'
    result.raise_for_status()


@router.post("/aerothermal/info", operation_id='post_aerothermal_info')
def post_aerothermal_info(aerothermal_info: AerothermalInfo):
    data = aerothermal_info.dict(exclude_none=True, exclude_unset=True, exclude={'house_type', 'climate_control_type'})
    data['message_notes'] = aerothermal_info.climate_control_system
    data['is_chalet'] = True if aerothermal_info.house_type == 'Una vivienda unifamiliar' else False
    result = odoo_rest.send_post_api('contacts', data)
    if (result.ok):
        if (aerothermal_info.promotions):
            try:
                mailchimp.subscribe_to_mailchimp(aerothermal_info.email)
            except ApiClientError as error:
                raise HTTPException(status_code=error.status_code, detail=error.text)
        return 'Succesfull request'
    result.raise_for_status()


@router.post(path="/otc/assistance", operation_id="post_otc_assistance")
def post_otc_assistance(otc_assistance: OTCAssistance) -> str | None:
    data = otc_assistance.dict(exclude_none=True, exclude_unset=True, exclude=OTC_ASSISTANCE_EXLCUDE_FIELDS)
    data['comment'] = (
        f'============== ACOMPAÑAMIENTO OTC - {datetime.now()} ==============\n'
        f'Página web: {otc_assistance.website}\n'
        f'Redes sociales: {otc_assistance.social_media}\n'
        f'¿La comunidad energética está ya constituida?: {otc_assistance.status}\n'
        f'Indica la forma jurídica: {otc_assistance.legal_form}\n'
        f'Tipo de municipio donde se desarrolla la actividad: {otc_assistance.size}\n'
        f'¿Quién está impulsando la iniciativa?: {otc_assistance.sponsors}\n'
        f'¿Cuántas personas hay en el grupo directivo o comité?: {otc_assistance.commite_size}\n'
        f'¿Tiene representante de los siguientes sectores?: {otc_assistance.representation}\n'
        f'¿Cuáles son los objetivos del grupo?: {otc_assistance.objectives}\n'
        f'¿Cuál es la principal área de interés?: {otc_assistance.main_interests}\n'
        f'¿Y la segunda?: {otc_assistance.secondary_interests}\n'
        f'¿Cómo ha conocido la existencia de esta OTC?: {otc_assistance.about_us}\n'
    )
    data['tags'], data['personal_data_policy'], data['promotions'] = map_strigified_fields(otc_assistance)

    result = odoo_rest.send_post_api(url='contacts', payload=data)
    if (result.ok):
        if (otc_assistance.promotions):
            try:
                mailchimp.subscribe_to_mailchimp(otc_assistance.email)
            except ApiClientError as error:
                raise HTTPException(status_code=error.status_code, detail=error.text)
        return 'Succesfull request'
    result.raise_for_status()


@router.post(path="/energy-certificate", operation_id="post_energy_certificate")
def post_energy_certificate(energy_certificate: EnergyCertificate):
    data = energy_certificate.dict(exclude_none=True, exclude_unset=True, exclude=ENERGY_CERTIFICATE_EXCLUDE_FIELDS)
    data['comment'] = _build_energy_certificate_comment(energy_certificate)
    data['tags'], data['personal_data_policy'], data['promotions'] = map_strigified_fields(energy_certificate)

    result = odoo_rest.send_post_api(url='contacts', payload=data)
    if (result.ok):
        if (energy_certificate.promotions):
            try:
                mailchimp.subscribe_to_mailchimp(energy_certificate.email)
            except ApiClientError as error:
                raise HTTPException(status_code=error.status_code, detail=error.text)
        return 'Succesfull request'
    result.raise_for_status()


def _build_energy_certificate_comment(energy_certificate: EnergyCertificate):
    comment = (
        '=================== CERTIFICADO DE EFICIENCIA ENERGÉTICA ===================\n'
        f'Propiedad de la vivienda: {energy_certificate.home_ownership}\n'
        f'Referencia catastral: {energy_certificate.cadastral_reference}\n'
        f'Número de personas viviendo en el hogar: {energy_certificate.number_of_residents}\n'
        'VENTANAS\n'
        f'Número de ventanas pequeñas al Norte: {energy_certificate.n_small_window_north}\n'
        f'Número de ventanas pequeñas al Sur: {energy_certificate.n_small_window_south}\n'
        f'Número de ventanas pequeñas al Este: {energy_certificate.n_small_window_east}\n'
        f'Número de ventanas pequeñas al Oeste: {energy_certificate.n_small_window_west}\n'
        f'Número de ventanas medianas al Norte: {energy_certificate.n_medium_window_north}\n'
        f'Número de ventanas medianas al Sur: {energy_certificate.n_medium_window_south}\n'
        f'Número de ventanas medianas al Este: {energy_certificate.n_medium_window_east}\n'
        f'Número de ventanas medianas al Oeste: {energy_certificate.n_medium_window_west}\n'
        f'Número de ventanas grandes al Norte: {energy_certificate.n_big_window_north}\n'
        f'Número de ventanas grandes al Sur: {energy_certificate.n_big_window_south}\n'
        f'Número de ventanas grandes al Este: {energy_certificate.n_big_window_east}\n'
        f'Número de ventanas grandes al Oeste: {energy_certificate.n_big_window_west}\n'
        f'Número de puertas acristaladas al Norte: {energy_certificate.n_glassed_doors_north}\n'
        f'Número de puertas acristaladas al Sur: {energy_certificate.n_glassed_doors_south}\n'
        f'Número de puertas acristaladas al Este: {energy_certificate.n_glassed_doors_east}\n'
        f'Número de puertas acristaladas al Oeste: {energy_certificate.n_glassed_doors_west}\n'
        f'Tipo de ventana: {energy_certificate.window_types}\n'
        f'Descripción y ubicación de tipos de ventana: {energy_certificate.window_description}\n'
        f'Tipo de vidrio: {energy_certificate.glass_types}\n'
        f'Descripción y ubicación de tipos de vidrio: {energy_certificate.glass_description}\n'
        f'Tipo de marco: {energy_certificate.frame_types}\n'
        f'Descripción y ubicación de tipos de marco: {energy_certificate.frame_description}\n'
        f'Información extra sobre las ventanas: {energy_certificate.window_extra_info}\n'
        'BALCONES\n'
        f'Número de terrazas cerradas/incroporadas al Norte: {energy_certificate.n_balcony_north}\n'
        f'Número de terrazas cerradas/incroporadas al Sur: {energy_certificate.n_balcony_south}\n'
        f'Número de terrazas cerradas/incroporadas al Este: {energy_certificate.n_balcony_east}\n'
        f'Número de terrazas cerradas/incroporadas al Oeste: {energy_certificate.n_balcony_west}\n'
        'EQUIPOS DE CALEFACCIÓN, REFRIGERACIÓN Y AGUA CALIENTE SANITARIA\n'
        f'Sistema de calefacción principal: {energy_certificate.heating_system}\n'
        f'Tipo de calefacción: {energy_certificate.heating_type}\n'
        f'¿Es el mismo equipo para calefacción y agua caliente sanitaria?: {energy_certificate.heating_use}\n'
        f'Antigüedad de los equipos de calefacción (nº de años): {energy_certificate.heating_age}\n'
        f'¿Qué potencia tiene el equipo de calefacción?: {energy_certificate.heating_power}\n'
        f'¿Tiene un depósito de acumulación de agua caliente aparte?: {energy_certificate.water_tank}\n'
        f'Números de equipos de calefacción: {energy_certificate.n_heating_devices}\n'
        f'Tipo de Agua Caliente Sanitaria (ACS).( Si va por separado): {energy_certificate.water_heating_type}\n'
        f'Antigüedad de los equipos de agua caliente sanitaria (nº de años): {energy_certificate.water_heating_age}\n'
        f'¿Qué potencia tiene el equipo de ACS?: {energy_certificate.water_heating_power}\n'
        f'¿Tiene un depósito de acumulación aparte?: {energy_certificate.accumulation_tank}\n'
        f'Tipo de refrigeración: {energy_certificate.refrigeration_type}\n'
        f'Número de equipos de refrigeración (nº de máquinas exteriores): {energy_certificate.n_refrigeration_devices}\n'
        f'Antigüedad de los equipos de refrigeración (nº de años): {energy_certificate.refrigeration_age}\n'
        '============================================================================'
    )
    return comment
