import logging

from fastapi import APIRouter

from src.models.gifts import Gift, GiftContact, GiftContactMinor
from src.utils import odoo_rest
from src.utils.logged_router import LoggedRoute

router = APIRouter(
    prefix="/gifts",
    tags=["Gifts", "Contacts", "Odoo"],
    route_class=LoggedRoute
)

logger = logging.getLogger('uvicorn.error')

CONTACTS_ENDPOINT = "contacts"


@router.post("/create", operation_id='post_gift')
def post_gift(gift: Gift):
    logger.info(f'ROUTERS/GIFTS - /create | Giver: {gift.email} | Recipient: {gift.recipient_email}')
    send_giver_data(gift)
    send_recipient_data(gift)
    return 'Ok'


def send_giver_data(gift: Gift):
    giver_data = build_giver_data(gift)
    giver_response = send_contact_data(giver_data)
    logger.info(f'ROUTERS/GIFTS - Giver response: {giver_response}')


def send_recipient_data(gift: Gift):
    recipient = build_recipient_data(gift)
    recipient_response = send_contact_data(recipient)
    logger.info(f'ROUTERS/GIFTS - Recipient response: {recipient_response}')


def build_giver_data(gift: Gift):
    giver_data = gift.dict()
    giver_data['message_notes'] = build_gift_message(gift)
    return giver_data


def build_recipient_data(gift: Gift):
    return {
        'firstname': gift.recipient_firstname,
        'lastname': gift.recipient_lastname,
        'email': gift.recipient_email,
        'message_notes': build_gift_message(gift),
    }


def build_gift_message(gift: Gift):
    is_minor_str = 'Sí' if gift.is_minor else 'No'
    message = f"Regalo de [{gift.email}] para [{gift.recipient_email}]: {gift.amount}€ en {gift.plant} tipo {gift.type[0]}; Menor de edad: {is_minor_str}"
    logger.debug(f'ROUTERS/GIFTS - Gift message: {message}')
    return message


def send_contact_data(contact: dict):
    response = odoo_rest.send_post_api(CONTACTS_ENDPOINT, contact)
    return odoo_rest.__handle_odoo_respone(response)


@router.post("/update/", operation_id='update_gift')
def update_gift(contact: GiftContactMinor | GiftContact):
    if type(contact) is GiftContactMinor and contact.is_minor:
        response = send_minor_tutor_data(contact)
    else:
        contact_dict = build_gift_contact(contact)
        response = send_contact_data(contact_dict)
    return response


def send_minor_tutor_data(contact: GiftContactMinor):
    tutor = build_tutor_gift_contact(contact)
    tutor_response = send_contact_data(tutor)
    if 'id' in tutor_response:
        minor = build_minor_gift_contact(contact, tutor_response['id'])
        return send_contact_data(minor)


def build_gift_contact(contact: GiftContact):
    return contact.dict(exclude_unset=True, exclude={'tutor_firstname', 'tutor_lastname', 'tutor_vat', 'tutor_gender', 'tutor_birthdate'})


def build_tutor_gift_contact(contact: GiftContactMinor):
    tutor = {
        'firstname': contact.tutor_firstname,
        'lastname': contact.tutor_lastname,
        'vat': contact.tutor_vat,
        'gender': contact.tutor_gender,
        'birthdate': contact.tutor_birthdate
    }
    return tutor


def build_minor_gift_contact(contact: GiftContact, tutor_id: int):
    minor = build_gift_contact(contact)
    minor['tutor'] = tutor_id
    return minor
