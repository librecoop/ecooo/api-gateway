from fastapi import APIRouter, HTTPException
from mailchimp_marketing.api_client import ApiClientError

from src.config import settings
from src.models.contact import Contact
from src.utils import mailchimp, odoo_rest
from src.utils.logged_router import LoggedRoute

router = APIRouter(
    prefix="/economic_activisim_school",
    tags=["EAE", "Odoo"],
    route_class=LoggedRoute
)


@router.post("/candidates", operation_id='post_candidate')
def post_candidate(contact: Contact):
    result = odoo_rest.send_post('partner/create', contact)['result']
    if (int(result.get('code')) == 200):
        if (contact.promotions):
            try:
                res=mailchimp.subscribe_to_mailchimp(contact.email, 'eae')
            except ApiClientError as error:
                raise HTTPException(status_code=error.status_code, detail=error.text)
        return result
    raise HTTPException(status_code=int(result.get('code')), detail=result.get('error'))
