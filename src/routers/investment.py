from fastapi import APIRouter, HTTPException
from mailchimp_marketing.api_client import ApiClientError

from src.models.investment import Investment
from src.utils import mailchimp, odoo_rest
from src.utils.logged_router import LoggedRoute

router = APIRouter(
    prefix="/investments",
    tags=["Investments", "Odoo"],
    route_class=LoggedRoute
)


@router.post("/", operation_id='post_investment')
def post_investment(investment: Investment):
    # Ensure correct format of both vat before sending to Odoo
    if investment.vat:
        investment.vat = investment.vat.upper().replace(' ', '').replace('-', '')
    if investment.vat2:
        investment.vat2 = investment.vat2.upper().replace(' ', '').replace('-', '')
    # If minor and tutor share vat add vat suffix to avoid duplication
    if investment.type == 'minor' and investment.vat == investment.vat2:
        investment.vat = f'{investment.vat}_menor_{investment.name}'
    result = odoo_rest.send_post('contract/create', investment)['result']
    if (int(result.get('code')) == 200):
        if (investment.promotions):
            try:
                mailchimp.subscribe_to_mailchimp(investment.email)
            except ApiClientError as error:
                raise HTTPException(status_code=error.status_code, detail=error.text)
        return result
    raise HTTPException(status_code=int(result.get('code')), detail=result.get('error'))
