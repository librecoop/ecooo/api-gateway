from json.decoder import JSONDecodeError

import requests
from fastapi import APIRouter, HTTPException, Request

from src.config import settings
from src.utils.logged_router import LoggedRoute

router = APIRouter(tags=['Proxy'], route_class=LoggedRoute)

@router.api_route('/allocations/',                 methods=['GET'])
@router.api_route('/contracts/',                   methods=['GET', 'PUT', 'POST'])
@router.api_route('/contracts/{_id}',              methods=['GET', 'PUT'])
@router.api_route('/user/',                        methods=['GET', 'PUT'])
@router.api_route('/bank_account/',                methods=['GET', 'POST'])
@router.api_route('/bank_account/{_id}',           methods=['GET', 'PUT', 'DELETE'])
@router.api_route('/info/countries',               methods=['GET'])
@router.api_route('/info/states',                  methods=['GET'])
@router.api_route('/info/states_by_country/{_id}', methods=['GET'])
@router.api_route('/info/person_types',            methods=['GET'])
@router.api_route('/powerstation/{_id}',           methods=['GET'])
async def catch_all(req: Request):
    try:
        data = await req.json()
    except JSONDecodeError:
        data = None

    # We remove the host header otherwise the nginx infront of the odoo returns a error 502 bad gateway
    mutable_headers = req.headers.mutablecopy()
    del mutable_headers['host']
    del mutable_headers['Accept-Language']
    res = requests.request(
        req.method,
        f'{settings["odoo"]["url"]}/api{req.url.path}',
        headers=mutable_headers,
        params=req.query_params,
        json=data,
    )
    if res.ok:
        return res.json()
    elif res.status_code == 401:
        raise HTTPException(status_code=res.status_code, detail=res.text)
    else:
        raise HTTPException(status_code=res.status_code, detail=res.text)
