import logging

import requests
from fastapi import APIRouter, HTTPException
from jinja2 import Environment, PackageLoader, select_autoescape

from src.config import settings
from src.models.account import Login, Signup, SignupRequest
from src.utils import odoo_rest

logger = logging.getLogger('uvicorn.error')

router = APIRouter(
    prefix='/account',
    tags=['Account'],
)

jnj_env = Environment(loader=PackageLoader('src'), autoescape=select_autoescape())


@router.post('/signup_request', operation_id='signup_request')
def signup_request(signup_request: SignupRequest):
    res = odoo_rest.send_post_api('account/signup_request', {'vat': signup_request.vat})
    if res.ok:
        data = res.json()

        txt_template = jnj_env.get_template('signup_request.txt.j2')
        html_template = jnj_env.get_template('signup_request.html.j2')

        mg_data = {
            'from': 'Ecooo <contacto@ecooo.es>',
            'to': data['email'],
            'subject': 'Confirma el acceso al área personal de Ecooo',
            'text': txt_template.render(recipient_name=data['name'], signup_token=data['token']),
            'html': html_template.render(recipient_name=data['name'], signup_token=data['token']),
        }
        _send_mailgun(mg_data)
    else:
        logger.error(f'Account creation for vat {signup_request.vat} failed. Response code {res.status_code}, message: {res.text}')
    # Send ok even if it failed to not leak if vat exists
    return 'ok'


@router.post('/signup', operation_id='signup')
def signup(signup: Signup):
    res = odoo_rest.send_post_api('account/signup', {'token': signup.token, 'password': signup.password})
    if res.ok:
        data = res.json()

        txt_template = jnj_env.get_template('signup.txt.j2')
        html_template = jnj_env.get_template('signup.html.j2')

        mg_data = {
            'from': 'Ecooo <contacto@ecooo.es>',
            'to': data['email'],
            'subject': 'Confirmación de cuenta de área personal de Ecooo',
            'text': txt_template.render(recipient_name=data['name'], login=data['login']),
            'html': html_template.render(recipient_name=data['name'], login=data['login']),
        }
        _send_mailgun(mg_data)

        return data['jwt_token']

    raise HTTPException(status_code=res.status_code, detail=res.text)


@router.post('/login', operation_id='login')
def login(login: Login):
    res = odoo_rest.send_post_api('account/login', {'vat': login.vat, 'password': login.password})
    if res.ok:
        return res.json()

    raise HTTPException(status_code=res.status_code, detail=res.text)


def _send_mailgun(data):
    response = requests.post(
        f'{settings["mailgun"]["url"]}/v3/{settings["mailgun"]["domain"]}/messages',
        auth=('api', settings['mailgun']['key']),
        data=data)
    logger.info(f'Sent email to {data["to"]} and got response {response.text}')
    if response.ok:
        return response
    raise HTTPException(status_code=response.status_code, detail=response.text)
