from json.decoder import JSONDecodeError
import logging
import requests
from fastapi import APIRouter, HTTPException, Request
from fastapi.responses import StreamingResponse
from src.config import settings
from src.utils.logged_router import LoggedRoute

router = APIRouter(tags=['Allocations'], route_class=LoggedRoute)


logger = logging.getLogger('uvicorn.error')

@router.api_route('/allocations/{id}/report', methods=['GET'], responses={
        200: {
            "content": {"application/pdf": {}},
            "description": "Return a pdf file.",
        }
    },
    openapi_extra={
        "parameters": [{
            "in": "path",
            "name": "id",
            "required": True
        }]
    })
async def get_allocation_report(req: Request):
    res = requests.request(
        'GET',
        f'{settings["odoo"]["url"]}/api{req.url.path}',
        headers=req.headers,
        params=req.query_params,
        stream=True
    )

    def iterfile():
        for chunk in res.iter_content(chunk_size=1024):
            yield chunk

    headers = {key: value for key, value in res.headers.items() if key.lower() != 'content-encoding'}

    if res.ok:
        return StreamingResponse(iterfile(), headers=headers, status_code=res.status_code)
    elif res.status_code == 401:
        raise HTTPException(status_code=res.status_code, detail=res.text)
    else:
        raise HTTPException(status_code=res.status_code, detail=res.text)