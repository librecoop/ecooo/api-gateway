from fastapi import APIRouter, HTTPException
from mailchimp_marketing.api_client import ApiClientError

from src.config import settings
from src.models.subscriber import Subscriber
from src.utils import mailchimp
from src.utils.logged_router import LoggedRoute

router = APIRouter(
    prefix="/newsletter",
    tags=["Newsletter"],
    route_class=LoggedRoute
)


@router.post("/subscriber", operation_id='post_subscirber')
def post_subscriber(subscriber: Subscriber):
    try:
        mailchimp.subscribe_to_mailchimp(subscriber.email)
        return 'Subscriber added correctly'
    except ApiClientError as error:
        return HTTPException(status_code=error.status_code, detail=error.text)
