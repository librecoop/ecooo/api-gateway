import json

import requests
from fastapi import APIRouter, HTTPException, Request
from mailchimp_marketing.api_client import ApiClientError

from src.config import settings
from src.models.contact import AllowPromotions, Contact, ContactV2
from src.utils.mappers import map_strigified_fields
from src.utils import mailchimp, odoo_rest
from src.utils.logged_router import LoggedRoute

router = APIRouter(
    prefix="/contacts",
    tags=["Contacts", "Odoo"],
    route_class=LoggedRoute
)

routerClassic = APIRouter(
    prefix="/contacts",
    tags=["Contacts", "Odoo"]
)


@router.post("/", operation_id='post_contact')
def post_contact(contact: Contact):
    result = odoo_rest.send_post('partner/create', contact)['result']
    if (int(result.get('code')) == 200):
        if (contact.promotions):
            try:
                mailchimp.subscribe_to_mailchimp(contact.email)
            except ApiClientError as error:
                raise HTTPException(status_code=error.status_code, detail=error.text)
        return result
    raise HTTPException(status_code=int(result.get('code')), detail=result.get('error'))

@router.post("/v2", operation_id='post_contact_v2')
def post_contact_v2(contact: ContactV2):
    contact_dict = contact.dict(exclude_none=True, exclude={'tags_str', 'personal_data_policy_str', 'promotions_str'})
    contact_dict['tags'], contact_dict['personal_data_policy'], contact_dict['promotions'] = map_strigified_fields(contact)
    
    response = odoo_rest.send_post_api('contacts', contact_dict)
    if response.ok:
        if (contact.promotions):
            try:
                mailchimp.subscribe_to_mailchimp(contact.email)
            except ApiClientError as error:
                raise HTTPException(status_code=error.status_code, detail=error.text)
    return odoo_rest.__handle_odoo_respone(response);
        

@router.post("/allow_promotions", operation_id='post_allow_promotions')
def post_allow_promotions(allow_promotions: AllowPromotions):
    res = odoo_rest.send_post_api('user/allow_promotions', { 'id': allow_promotions.id, 'allow_promotions': allow_promotions.allow_promotions })
    if (res.ok):
        if (allow_promotions.allow_promotions):
            mailchimp.subscribe_to_mailchimp(res.text.replace('"',''))
        else:
            mailchimp.unsubscribe_to_mailchimp(res.text.replace('"',''))
        return 'Update Successful'
    res.raise_for_status()

@routerClassic.api_route("/mailchimp_sync", methods=['GET', 'POST'])
async def post_mailchimp_sync(req: Request):
    if (req.method == 'POST'):
        data = await req.form()
    else:
        return 'ok' # This is a shitty hack because mailchimp needs a GET endpoint to validate the webhook 🤬

    res = requests.request(
        req.method,
        f'{settings["odoo"]["url"]}/api/user/mailchimp_sync',
        headers={'api_key': settings['odoo']['key']},
        params=req.query_params,
        json=json.loads(json.dumps(data._dict)),
    )
    if res.ok:
        return res.json()
    res.raise_for_status()
