from fastapi import APIRouter
from src.utils import odoo_rest
from src.models.info import Info

router = APIRouter(
    prefix="/info",
    tags=["Info", "Odoo"]
)


@router.get("/", operation_id='get_info', response_model=Info)
def get_info():
    response = odoo_rest.send_get('api/info/metrics')
    return Info.parse_obj(response)
