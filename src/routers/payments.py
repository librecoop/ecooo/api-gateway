from decimal import Decimal as D
from decimal import ROUND_HALF_UP
from fastapi import APIRouter
from pydantic import constr
from redsys.client import RedirectClient
from redsys.constants import EUR, STANDARD_PAYMENT

from src.config import settings

router = APIRouter(prefix="/payments", tags=["Payments"])


@router.get("/signature", operation_id="get_signature")
def get_signature(order: constr(min_length=9, max_length=9), amount: int):
    client = RedirectClient(settings["redsys"]["signature"])
    parameters = {
        "merchant_code": settings["redsys"]["merchant_code"],
        "terminal": settings["redsys"]["terminal"],
        "transaction_type": STANDARD_PAYMENT,
        "currency": EUR,
        "order": str(order),
        "amount": D(amount).quantize(D(".01"), ROUND_HALF_UP),
        "merchant_data": "Investment Payment",
        "merchant_name": "Ecooo",
        "titular": "Ecooo Energía Ciudadana S.COOP.",
        "product_description": f"Inversión de {amount}€ en Ecooo",
        "merchant_url": "https://ecooo.es/",
        "url_ok": f"{settings['area_personal']['url']}/inversiones?payment=successful",
    }
    args = client.prepare_request(parameters)
    return args
