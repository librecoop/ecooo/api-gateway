from enum import Enum
from typing import List

from fastapi import APIRouter, HTTPException

from src.models.project import PowerSationPublic, Project
from src.utils import odoo_rest

router = APIRouter(
    prefix="/projects",
    tags=["Projects", "Odoo"]
)

class ProjectType(str, Enum):
    shot_term = 'short_term',
    long_term = 'long_term'


@router.get("/", operation_id='get_projects', response_model=List[Project])
def get_projects():
    response = odoo_rest.send_get('projects')
    projects = []
    for project in response['response']:
        projects.append(Project.parse_obj(project))
    return projects

@router.get("/open", operation_id='get_open_projects', response_model=List[PowerSationPublic])
def get_open_projects(type: ProjectType):
    response = odoo_rest.send_get('api/powerstation', { 'type': type })
    return response


@router.get("/{project_id}", operation_id='get_project_by_id', response_model=Project)
def get_project(project_id):
    response = odoo_rest.send_get(f'projects/{project_id}')
    projects = response['response']
    if (len(projects) > 0):
        return Project.parse_obj(projects[0])
    raise HTTPException(
        status_code=404,
        detail=f'No project found with id {project_id}'
    )
