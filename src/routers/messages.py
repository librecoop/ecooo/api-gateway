import requests
from fastapi import APIRouter, HTTPException
from src.models.message import Message
from src.config import settings
from src.utils import odoo_rest
from src.utils.logged_router import LoggedRoute
from jinja2 import Environment, PackageLoader, select_autoescape

router = APIRouter(
    prefix="/messages",
    tags=["Messages"],
    route_class=LoggedRoute
)

jnj_env = Environment(loader=PackageLoader('src'), autoescape=select_autoescape())

EMAIL_TEMPLATES = {
    'payment-confirmation': {
        'template_name': 'payment_confirmation',
        'subject': 'Confirmación de pago - inversión en Ecoooo'
    },
    'transfer-reminder': {
        'template_name': 'transfer_reminder',
        'subject': 'Solo queda el último paso para invertir con Ecooo' 
    },
    'present-payment-confirmation': {
        'template_name': 'present_payment_confirmation',
        'subject': 'Confirmación de pago - inversión en Ecoooo'
    },
    'present-transfer-reminder': {
        'template_name': 'present_transfer_reminder',
        'subject': 'Solo queda el último paso para invertir con Ecooo' 
    },
    'course-transfer-reminder': {
        'template_name': 'course_transfer_reminder',
        'subject': 'Información para realizar el pago por transferencia de tu curso Guardabosques'
    }
}

@router.post("/", operation_id='post_message')
def post_message(message: Message):
    match message.product_mode:
        case 'short_term':
            mode = 'CORTO'
        case 'long_term':
            mode = 'LARGO'
        case _:
            mode = message.product_mode
    contacts = odoo_rest.send_get('partners', {'email': message.recipient_mail})
    if (len(contacts['response']) > 0):
        template = EMAIL_TEMPLATES.get(message.type)
        if template:
            data = {"from": "Ecooo <contacto@ecooo.es>", "to": message.recipient_mail}
            html_template = jnj_env.get_template(f"{template['template_name']}.html.j2")
            txt_template = jnj_env.get_template(f"{template['template_name']}.txt.j2")
            data["subject"] = template['subject']
        else:
            raise HTTPException(status_code=422, detail='Unkown message type, supported message types are payment-confirmation & transfer-reminder')
        data["text"] = txt_template.render(recipient_name=message.recipient_name, product_mode=mode, investment=message.investment)
        data["html"] = html_template.render(recipient_name=message.recipient_name, product_mode=mode, investment=message.investment)
        response = requests.post(
            f"{settings['mailgun']['url']}/v3/{settings['mailgun']['domain']}/messages",
            auth=("api", settings['mailgun']['key']),
            data=data)
        if response:
            return 'Message queued correctly'
        else:
            raise HTTPException(status_code=response.status_code, detail=response.text)
    else:
        raise HTTPException(status_code=400, detail='The recipient assinged is unknown')
