import yaml

with open('src/config.yml', 'r') as file:
    settings = yaml.safe_load(file)
