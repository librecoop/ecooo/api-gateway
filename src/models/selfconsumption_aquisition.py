from pydantic import BaseModel, EmailStr
from typing import Literal


class SelfconsumptionAquisition(BaseModel):
    firstname: str
    lastname: str
    phone: str
    email: EmailStr
    street: str
    street2: str | None
    city: str
    state: str
    zip: str
    building_role: str | None
    building_approval: str | None
    selconsumption_type: str | None
    selfconsumption_aquisition: str | None
    tags: list[str] | None = None
    participation_reason: str | None = None
    about_us: Literal["Redes Sociales", "Prensa", "Búsqueda de internet", "Amigo/Familia", "Charla/Evento", "Otro"]
    personal_data_policy: bool
    promotions: bool = False
