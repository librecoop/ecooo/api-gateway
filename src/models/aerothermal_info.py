from pydantic import BaseModel, EmailStr
from typing import Literal


class AerothermalInfo(BaseModel):
    firstname: str
    lastname: str
    phone: str
    email: EmailStr
    street: str
    state: str
    zip: str
    house_type: str
    climate_control_system: str
    tags: list[str] | None = None
    participation_reason: str | None = None
    about_us: Literal["Redes Sociales", "Prensa", "Búsqueda de internet", "Amigo/Familia", "Charla/Evento", "Otro"] | None
    personal_data_policy: bool
    promotions: bool = False
