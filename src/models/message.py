from typing import Literal
from pydantic import BaseModel


class Message(BaseModel):
    type: Literal['payment-confirmation', 'transfer-reminder', 'present-payment-confirmation', 'present-transfer-reminder', 'course-transfer-reminder']
    recipient_mail: str
    recipient_name: str
    product_mode: str | None = None
    investment: int | None = None
