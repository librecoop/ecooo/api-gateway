from typing import Literal
from pydantic import BaseModel, EmailStr


class Investment(BaseModel):
    type: Literal['minor', 'individual', 'marriage', 'partnership']
    name: str
    surname: str | None = None
    vat: str
    gender: Literal['male', 'female', 'other'] | None = None
    birthdate: str | None = None
    phone: str
    email: EmailStr
    street: str
    street2: str | None = None
    is_chalet: bool = False
    city: str
    state: str
    zip: str
    country: str
    project: str
    inversion: int
    personal_data_policy: bool
    promotions: bool = False
    about_us: Literal["Redes Sociales", "Prensa", "Búsqueda de internet", "Amigo/Familia", "Charla/Evento", "Otro"] | None = None
    participation_reason: str | None = None
    name2: str | None = None
    surname2: str | None = None
    vat2: str | None = None
    gender2: Literal['male', 'female', 'other'] | None = None
    birthdate2: str | None = None
    tags: list[str] | None = None
