from pydantic import BaseModel, EmailStr

class Subscriber(BaseModel):
    email: EmailStr

