from pydantic import BaseModel, EmailStr
from typing import Literal


class AutoconsumptionEstimate(BaseModel):
    name: str
    surname: str
    phone: str
    email: EmailStr
    street: str
    street2: str
    city: str
    state: str
    zip: str
    aprox_consumption: int | None = None
    categories: list[str] | None = None
    participation_reason: str | None = None
    about_us: Literal["Redes Sociales", "Prensa", "Búsqueda de internet", "Amigo/Familia", "Charla/Evento", "Otro"] | None
    personal_data_policy: bool
    promotions: bool = False
