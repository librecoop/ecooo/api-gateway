from pydantic import BaseModel


class OTCAssistance(BaseModel):
    firstname:                  str
    street:                     str | None
    zip:                        str | None
    city:                       str
    email:                      str
    website:                    str | None
    social_media:               str | None
    status:                     str | None
    legal_form:                 str | None
    size:                       str | None
    sponsors:                   str | None
    commite_size:               str | None
    representation:             str | None
    main_interests:             str | None
    secondary_interests:        str | None
    about_us:                   str | None
    objectives:                 str | None
    personal_data_policy:       bool | None
    promotions:                 bool | None
    tags:                       list[str] | None
    #This fields are necessary to process entries comming from the webhooks addon of gravity forms, their are only used if the standard ones are not pressent
    promotions_str:             str | None
    personal_data_policy_str:   str | None
    tags_str:                   str | None
