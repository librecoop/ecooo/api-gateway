from pydantic import BaseModel, EmailStr
from typing import Literal


class Gift(BaseModel):
    firstname: str | None
    lastname: str | None
    phone: str | None
    vat: str | None
    email: EmailStr | None
    amount: float | None
    is_minor: bool | None
    plant: str | None
    recipient_firstname: str | None
    recipient_lastname: str | None
    recipient_email: EmailStr | None
    about_us: (
        Literal[
            "Redes Sociales",
            "Prensa",
            "Búsqueda de internet",
            "Amigo/Familia",
            "Charla/Evento",
            "Otro",
        ]
        | None
    ) = None
    participation_reason: str | None = None
    personal_data_policy: bool
    promotions: bool
    type: list[str] | None = None


class GiftContact(BaseModel):
    firstname: str
    lastname: str
    vat: str
    email: EmailStr
    gender: str
    birthdate: str
    phone: str
    street: str
    city: str
    state: str
    zip: str
    country: str
    personal_data_policy: bool
    promotions: bool | None = False


class GiftContactMinor(GiftContact):
    is_minor: bool = False
    tutor_firstname: str
    tutor_lastname: str
    tutor_vat: str
    tutor_gender: str
    tutor_birthdate: str
