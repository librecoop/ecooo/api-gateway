from pydantic import BaseModel, EmailStr
from typing import Literal


class Contact(BaseModel):
    name: str
    surname: str
    phone: str
    email: EmailStr
    personal_data_policy: bool
    promotions: bool = False
    street: str | None = None
    street2: str | None = None
    is_chalet: bool | None = None
    city: str | None = None
    state: str | None = None
    zip: str | None = None
    categories: list[str] | None = None
    self_consumption: str | None = None
    about_us: Literal["Redes Sociales", "Prensa", "Búsqueda de internet", "Amigo/Familia", "Charla/Evento", "Otro"] | None = None
    participation_reason: str | None = None
    related: str | None = None
    subject: str | None = None
    inquiry: str | None = None
    gender: Literal['male', 'female', 'other'] | None = None
    birthdate: str | None = None
    country: str | None


class ContactV2(BaseModel):
    id:                         int | None
    firstname:                  str | None
    lastname:                   str | None
    street:                     str | None
    additional_street:          str | None
    zip:                        str | None
    city:                       str | None
    state_id:                   int | None
    country_id:                 int | None
    email:                      str | None
    phone:                      str | None
    mobile:                     str | None
    alias:                      str | None
    vat:                        str | None
    gender:                     Literal['male', 'female', 'other'] | None = None
    birthday:                   str | None
    about_us:                   Literal["Redes Sociales", "Prensa", "Búsqueda de internet", "Amigo/Familia", "Charla/Evento", "Otro"] | None = None
    comment:                    str | None
    personal_data_policy:       bool | None
    promotions:                 bool | None
    message_notes:              str | None # Notes
    tags:                       list[str] | None
    #This fields are necessary to process entries comming from the webhooks addon of gravity forms, their are only used if the standard ones are not pressent
    promotions_str:             str | None
    personal_data_policy_str:   str | None
    tags_str:                   str | None

class AllowPromotions(BaseModel):
    id: int
    allow_promotions: bool
