from datetime import date

from pydantic import BaseModel


class Project(BaseModel):
    id: int
    reservation: float
    name: str
    name_display: str
    objective: float
    city: str
    investors: int
    energy_generated: float | None = None
    rated_power: float | None = None
    province: str
    description: str
    amount_assigned: float
    station_image_url: str | None = None

class PowerSationPublic(BaseModel):
    id: int
    name: str
    display_name: str | None
    image: str | None 
    province: str
    city: str
    link_google_maps: str
    peak_power: str
    rated_power: str
    start_date: date
    energy_generated: float
    tn_co2_avoided: float
    reservation: float
    contracts_count: int
    eq_family_consumption: float
