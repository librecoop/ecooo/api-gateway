from pydantic import BaseModel

class SignupRequest(BaseModel):
    vat: str

class Signup(BaseModel):
    token: str
    password: str

class Login(BaseModel):
    vat: str
    password: str
