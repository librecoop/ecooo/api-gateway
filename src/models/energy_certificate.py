from pydantic import BaseModel, EmailStr


class EnergyCertificate(BaseModel):

    firstname: str
    lastname: str
    vat: str
    street: str
    city: str
    state: str
    zip: str
    email: EmailStr
    phone: str
    personal_data_policy: bool | None
    promotions: bool | None
    tags: list[str] | None
    #This fields are necessary to process entries comming from the webhooks addon of gravity forms, their are only used if the standard ones are not pressent
    promotions_str: str | None
    personal_data_policy_str: str | None
    tags_str: str | None

    home_ownership: str
    cadastral_reference: str | None
    number_of_residents: int

    # Small window information
    n_small_window_north: int
    n_small_window_south: int
    n_small_window_east: int
    n_small_window_west: int
    # Medium window information
    n_medium_window_north: int
    n_medium_window_south: int
    n_medium_window_east: int
    n_medium_window_west: int
    # Big window information
    n_big_window_north: int
    n_big_window_south: int
    n_big_window_east: int
    n_big_window_west: int
    n_glassed_doors_north: int
    n_glassed_doors_south: int
    n_glassed_doors_east: int
    n_glassed_doors_west: int

    window_types: str
    window_description: str | None

    glass_types: str
    glass_description: str | None

    frame_types: str
    frame_description: str | None

    window_extra_info: str | None

    # Balcony information
    n_balcony_north: int
    n_balcony_south: int
    n_balcony_east: int
    n_balcony_west: int

    # Heating infromation
    heating_system: str
    heating_type: str
    heating_use: str
    heating_age: str
    heating_power: str
    water_tank: str | None
    n_heating_devices: str
    water_heating_type: str | None
    water_heating_age: str
    water_heating_power: str | None
    accumulation_tank: str | None

    # Refrigeration information
    refrigeration_type: str
    n_refrigeration_devices: str | None
    refrigeration_age: str | None
