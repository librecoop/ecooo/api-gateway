from src.models.contact import ContactV2
from src.models.otc_assistance import OTCAssistance
from src.models.energy_certificate import EnergyCertificate


def map_strigified_fields(input: ContactV2 | OTCAssistance | EnergyCertificate):
    if input.tags is None and input.tags_str:
        tags = [tag.strip() for tag in input.tags_str.split(',')]
    else:
        tags = input.tags
    if input.personal_data_policy is None and input.personal_data_policy_str:
        personal_data_policy = True
    else:
        personal_data_policy = input.personal_data_policy
    if input.promotions is None and input.promotions_str:
        promotions = True
    else:
        promotions = input.promotions
    return tags, personal_data_policy, promotions
