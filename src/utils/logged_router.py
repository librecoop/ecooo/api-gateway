from typing import Callable
from fastapi import Request, Response
from fastapi.routing import APIRoute
import logging

from requests import JSONDecodeError

logger = logging.getLogger('uvicorn.error')


class LoggedRoute(APIRoute):
    def get_route_handler(self) -> Callable:
        original_route_handler = super().get_route_handler()

        async def custom_route_handler(request: Request) -> Response:
            response: Response = await original_route_handler(request)
            if (request.method == 'POST' or request.method == 'PUT'):
                try:
                    json = await request.json()
                    logger.info(f'{request.method} {request.url.path} - {json} - {response.status_code} - {response.body}')
                except JSONDecodeError:
                    logger.exception()
            return response

        return custom_route_handler
