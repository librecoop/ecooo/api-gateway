from typing import Any
from pydantic import BaseModel
from fastapi import HTTPException, Response
from src.config import settings
import requests


def send_get(url, params={}):
    response = requests.get(
        f"{settings['odoo']['url']}/{url}",
        headers={'api_key': settings['odoo']['key']},
        params=params
    )
    return __handle_odoo_respone(response)


def send_post(url: str, payload: BaseModel):
    data = {'params': payload.dict(exclude_none=True)}
    response = requests.post(
        f"{settings['odoo']['url']}/{url}",
        json=data,
        headers={'api_key': settings['odoo']['key']}
    )
    return __handle_odoo_respone(response)


def send_post_api(url: str, payload: dict) -> Response:
    response = requests.post(
        f"{settings['odoo']['url']}/api/{url}",
        json=payload,
        headers={'api_key': settings['odoo']['key']}
    )
    return response


def __handle_odoo_respone(response: requests.Response) -> Any:
    if response.ok:
        return response.json()
    raise HTTPException(status_code=response.status_code, detail=response.text)
