import mailchimp_marketing as MailchimpMarketing

from src.config import settings


def subscribe_to_mailchimp(email: str, list_id: str = 'default'):
    client = MailchimpMarketing.Client()
    client.set_config({
        "api_key": settings['mailchimp']['key'],
        "server": settings['mailchimp']['server_prefix']
    })
    return client.lists.batch_list_members(settings['mailchimp']['list_id'][list_id], {"members": [{'email_address': email, 'status': 'subscribed'}]})

def unsubscribe_to_mailchimp(email: str, list_id: str = 'default'):
    client = MailchimpMarketing.Client()
    client.set_config({
        "api_key": settings['mailchimp']['key'],
        "server": settings['mailchimp']['server_prefix']
    })

    return client.lists.batch_list_members(settings['mailchimp']['list_id'][list_id], {"members": [{'email_address': email, 'status': 'unsubscribed'}], 'update_existing': True})

