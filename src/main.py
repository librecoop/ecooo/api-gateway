from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from .config import settings
from .routers import (autoconsumption, contacts, gifts, info, investment, messages, projects, account, proxy, newsletter, economic_activism_school,
                      payments, allocations)


app = FastAPI(
    title='Ecooo API Gateway',
    version='1.7.2',
    docs_url=settings.get('docs_url'),
    redoc_url=settings.get('redoc_url'))

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(autoconsumption.router)
app.include_router(contacts.router)
app.include_router(contacts.routerClassic)
app.include_router(gifts.router)
app.include_router(info.router)
app.include_router(investment.router)
app.include_router(messages.router)
app.include_router(newsletter.router)
app.include_router(projects.router)
app.include_router(account.router)
app.include_router(payments.router)
app.include_router(economic_activism_school.router)
app.include_router(allocations.router)
app.include_router(proxy.router)
